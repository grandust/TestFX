using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static class Facade
{
    public static UIMain UI => UIMain.main;
    public static GameManager GM => GameManager.Instance;
}
