﻿using UnityEngine;

namespace Utils
{
    public static class GameObjectHelper
    {
        public static T FindUpToHierarchy<T>(this GameObject o ) where T : class 
        {
            var obj = o.transform.parent;

            while (obj != null)
            {
                var i = obj.gameObject.GetComponent<T>();

                if (i != null)
                {
                    return i;
                }

                obj = obj.parent;
            }

            return null;
        }
        
        public static Transform FindDeepChild(this Transform aParent, string aName)
        {
            var result = aParent.Find(aName);
            if (result != null)
                return result;
            foreach (Transform child in aParent)
            {
                result = child.FindDeepChild(aName);
                if (result != null)
                    return result;
            }
            return null;
        }
        
        public static void SetParent(this GameObject obj, string parentName)
        {
            if (parentName != null)
            {
                var parentGO = GameObject.Find(parentName);
                if (parentGO == null)
                {
                    parentGO = new GameObject();
                    parentGO.name = parentName;
                    parentGO.transform.parent = null;
                }
                obj.transform.SetParent(parentGO.transform, false);
            }
        }

    }
}