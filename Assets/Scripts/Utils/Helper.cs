﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Utils
{
    public static partial class Helper
    {
        public static void Shuffle<T>(this IList<T> ts)
        {
            var count = ts.Count;
            var last = count - 1;
            for (var i = 0; i < last; ++i) {
                var r = UnityEngine.Random.Range(i, count);
                var tmp = ts[i];
                ts[i] = ts[r];
                ts[r] = tmp;
            }
        }

        public static T Random<T>(this IEnumerable<T> temp)
        {
            return temp.ElementAt(UnityEngine.Random.Range(0, temp.Count()));
        }

        public static int GetRandomIndexByWeight(this List<float> prob)
        {
            var sum = prob.Sum();
            var s = UnityEngine.Random.Range(0, sum);
            for (var i = 0; i < prob.Count; i++) {
                if (s < prob[i])
                    return i;
                s -= prob[i];
            }
            return 0;
        }

        public static IEnumerator Crt(float time, Action<float> func)
        {
            var f = 0f;
            while (f < time) {
                func.Invoke(f / time);
                f += Time.deltaTime;
                yield return 0;
            }
            func.Invoke(1);
        }

        public static IEnumerable<Type> FindInheritorTypes<T>()
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .Where(x => x.IsClass && !x.IsAbstract && x.IsSubclassOf(typeof(T)));
        }

        public static IEnumerable<T> GetInterfaces<T>(this GameObject inObj) where T : class
        {
            return inObj.GetComponents<Component>().OfType<T>();
        }

        public static void Shuffle<T>(this List<T> list)
        {
            for (var i = 0; i < list.Count; i++) {
                var temp = list[i];
                var randomIndex = UnityEngine.Random.Range(i, list.Count);
                list[i] = list[randomIndex];
                list[randomIndex] = temp;
            }
        }

    }

    public struct Vector3Ext
    {
        public static Vector3 Mul(Vector3 a, Vector3 b) => new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
    }
}