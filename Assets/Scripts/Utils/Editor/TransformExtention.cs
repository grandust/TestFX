﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;

namespace Common.Utilities
{
public class TransformExtention {

    public static Vector3 tempPos = Vector3.zero;
    public static Quaternion tempRot = Quaternion.identity;
    public static Vector3 tempScale = Vector3.one;
    public static bool show = false;

    [CustomEditor(typeof(Transform))]
    public class TransformExtentionEditor : Editor
    {
        Transform _target;
        public override void OnInspectorGUI()
        {
            //base.OnInspectorGUI();
            if (_target == null) _target = (Transform)target;
            StandardTransformInspector();

            show = EditorGUILayout.Foldout(show, "Операции копирования/вставки");
            if (show)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label("Copy");
                if (GUILayout.Button("Pos"))
                {
                    tempPos = _target.position;
                }
                if (GUILayout.Button("Rot"))
                {
                    tempRot = _target.rotation;
                }
                if (GUILayout.Button("Scale"))
                {
                    tempScale = _target.lossyScale;
                }
                if (GUILayout.Button("LocalPos"))
                {
                    tempPos = _target.localPosition;
                }
                if (GUILayout.Button("LocalRot"))
                {
                    tempRot = _target.localRotation;
                }
                if (GUILayout.Button("LocalScale"))
                {
                    tempScale = _target.localScale;
                }
                GUILayout.EndHorizontal();
                GUILayout.BeginHorizontal();
                GUILayout.Label("Paste");
                if (GUILayout.Button("Pos"))
                {
                    Undo.RecordObject(_target, "Undo Pos");
                    _target.position = tempPos;
                }
                if (GUILayout.Button("Rot"))
                {
                    Undo.RecordObject(_target, "Undo Rot");
                    _target.rotation = tempRot;
                }
                if (GUILayout.Button("Scale"))
                {
                    Undo.RecordObject(_target, "Undo Scale");
                    _target.localScale = tempScale;
                }
                if (GUILayout.Button("LocalPos"))
                {
                    Undo.RecordObject(_target, "Undo LocalPos");
                    _target.localPosition = tempPos;
                }
                if (GUILayout.Button("LocalRot"))
                {
                    Undo.RecordObject(_target, "Undo LocalRot");
                    _target.localRotation = tempRot;
                }
                if (GUILayout.Button("LocalScale"))
                {
                    Undo.RecordObject(_target, "Undo LocalScale");
                    _target.localScale = tempScale;
                }
                GUILayout.EndHorizontal();
            }
        }

        private void StandardTransformInspector()
        {
            bool didPositionChange = false;
            bool didRotationChange = false;
            bool didScaleChange = false;

            // Watch for changes.
            //  1)  Float values are imprecise, so floating point error may cause changes
            //      when you've not actually made a change.
            //  2)  This allows us to also record an undo point properly since we're only
            //      recording when something has changed.

            // Store current values for checking later
            Vector3 initialLocalPosition = _target.localPosition;
            Vector3 initialLocalEuler = _target.localEulerAngles;
            Vector3 initialLocalScale = _target.localScale;

            EditorGUI.BeginChangeCheck();
            Vector3 localPosition = EditorGUILayout.Vector3Field("Position", _target.localPosition);
            if (EditorGUI.EndChangeCheck())
                didPositionChange = true;

            EditorGUI.BeginChangeCheck();
            Vector3 localEulerAngles = EditorGUILayout.Vector3Field("Rotation", _target.localEulerAngles);
            if (EditorGUI.EndChangeCheck())
                didRotationChange = true;

            EditorGUI.BeginChangeCheck();
            Vector3 localScale = EditorGUILayout.Vector3Field("Scale", _target.localScale);
            if (EditorGUI.EndChangeCheck())
                didScaleChange = true;

            // Apply changes with record undo
            if (didPositionChange || didRotationChange || didScaleChange)
            {
                Undo.RecordObject(_target, _target.name);

                if (didPositionChange)
                    _target.localPosition = localPosition;

                if (didRotationChange)
                    _target.localEulerAngles = localEulerAngles;

                if (didScaleChange)
                    _target.localScale = localScale;

            }

            Transform[] selectedTransforms = Selection.transforms;
            if (selectedTransforms.Length > 1)
            {
                foreach (var item in selectedTransforms)
                {
                    if (didPositionChange || didRotationChange || didScaleChange)
                        Undo.RecordObject(item, item.name);

                    if (didPositionChange)
                    {
                        item.localPosition = ApplyChangesOnly(
                            item.localPosition, initialLocalPosition, _target.localPosition);
                    }

                    if (didRotationChange)
                    {
                        item.localEulerAngles = ApplyChangesOnly(
                            item.localEulerAngles, initialLocalEuler, _target.localEulerAngles);
                    }

                    if (didScaleChange)
                    {
                        item.localScale = ApplyChangesOnly(
                            item.localScale, initialLocalScale, _target.localScale);
                    }

                }
            }
        }

        private Vector3 ApplyChangesOnly(Vector3 toApply, Vector3 initial, Vector3 changed)
        {
            if (!Mathf.Approximately(initial.x, changed.x))
                toApply.x = _target.localPosition.x;

            if (!Mathf.Approximately(initial.y, changed.y))
                toApply.y = _target.localPosition.y;

            if (!Mathf.Approximately(initial.z, changed.z))
                toApply.z = _target.localPosition.z;

            return toApply;
        }

    }

    [MenuItem("CONTEXT/Transform/Copy Position")]
    private static void CopyPosition(MenuCommand menuCommand)
    {
        var trans = menuCommand.context as Transform;
        tempPos = trans.position;
    }

    [MenuItem("CONTEXT/Transform/Paste Position")]
    private static void PastePosition(MenuCommand menuCommand)
    {
        
        var trans = menuCommand.context as Transform;
        trans.position = tempPos;
    }

    [MenuItem("CONTEXT/Transform/Copy Rotation")]
    private static void CopyRotation(MenuCommand menuCommand)
    {
        var trans = menuCommand.context as Transform;
        tempRot = trans.rotation;
    }

    [MenuItem("CONTEXT/Transform/Paste Rotation")]
    private static void PasteRotation(MenuCommand menuCommand)
    {

        var trans = menuCommand.context as Transform;
        trans.rotation = tempRot;
    }

    [MenuItem("CONTEXT/Transform/Copy Scale")]
    private static void CopyScale(MenuCommand menuCommand)
    {
        var trans = menuCommand.context as Transform;
        tempScale = trans.localScale;
    }

    [MenuItem("CONTEXT/Transform/Paste Scale")]
    private static void PasteScale(MenuCommand menuCommand)
    {

        var trans = menuCommand.context as Transform;
        trans.localScale = tempScale;
    }
}
}
#endif