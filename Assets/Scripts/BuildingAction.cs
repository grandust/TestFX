using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BuildingAction : IBuildingAction
{
    protected BuildingProperty props;
    protected Building building;

    public virtual void OnBuildComplete(Town t) { }
    public virtual void OnBuildDestroy(Town t) { }
    public virtual void StartProduction() { }


    public static BuildingAction Create(Building building, BuildingProperty p)
    {
        switch (p.propertyType) {
            case PropertyType.storage: return new BuildingActionStorage(building, p);
            case PropertyType.factory: return new BuildingActionFactory(building, p);
        }
        return null;
    }
}

