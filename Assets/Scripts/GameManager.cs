using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Facade;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Settings _settings;

    public Settings Settings => _settings;
    public static GameManager Instance { get; private set; }

    public Town CurrentTown;

    public bool IsDragEnable => UI["Ingame"].IsVisible;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        foreach(var r in Settings.resStart) {
            CurrentTown.Storage[r.resType] = r.resValue;
        }
        foreach (var r in Settings.resMax) {
            CurrentTown.StorageMax[r.resType] = r.resValue;
        }
        (UI["Ingame"] as Ingame).UpdateData(CurrentTown);
    }

    void Update()
    {
        
    }
}
