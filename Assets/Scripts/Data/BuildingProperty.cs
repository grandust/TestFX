using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public enum PropertyType
{
    storage,
    factory
}

public enum ResourceType
{
    gas,
    minerals
}

[System.Serializable]
public class ResourcePack
{
    public ResourceType resType;
    public int resValue;
}

[System.Serializable]
public class BuildingProperty
{
    public PropertyType propertyType;
    public ResourcePack resPack;
    public float factoryTime;
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(BuildingProperty))]
public class BuildingPropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        var pos = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        var row = 0;
        var typeRect = new Rect(pos.x, pos.y + 20 * row, pos.width, 20);
        EditorGUI.PropertyField(typeRect, property.FindPropertyRelative("propertyType"), GUIContent.none);
        row++;

        var resRect1 = new Rect(pos.x, pos.y + 20 * row, pos.width / 2, 20);
        var resRect2 = new Rect(pos.x + pos.width / 2, pos.y + 20 * row, pos.width / 2, 20);
        EditorGUI.PropertyField(resRect1, property.FindPropertyRelative("resPack").FindPropertyRelative("resType"), GUIContent.none);
        EditorGUI.PropertyField(resRect2, property.FindPropertyRelative("resPack").FindPropertyRelative("resValue"), GUIContent.none);
        row++;

        if (property.FindPropertyRelative("propertyType").intValue == (int)PropertyType.factory) {
            var tymeRect = new Rect(pos.x, pos.y + 20 * row, pos.width, 20);
            EditorGUI.PropertyField(tymeRect, property.FindPropertyRelative("factoryTime"), new GUIContent("Time"));
        }

        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty prop, GUIContent label)
    {
        return base.GetPropertyHeight(prop, label) * 5;
    }
}

#endif