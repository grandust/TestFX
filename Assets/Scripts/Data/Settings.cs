using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Game Settings")]
public class Settings : ScriptableObject
{
    public Vector2Int TownSize = new Vector2Int(40, 40);
    public List<BuildingData> buildings;

    [Header("Resources at start")]
    public List<ResourcePack> resStart;
    public List<ResourcePack> resMax;

    [Header("Game")]
    public float IndicatorUpdateTime;
}
