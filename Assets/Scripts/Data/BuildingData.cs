using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Building Data")]
public class BuildingData: ScriptableObject
{
    public string Title;
    public List<ResourcePack> buildCost;
    public float buildTime;
    public List<BuildingProperty> properties;
    public GameObject prefab;
}
