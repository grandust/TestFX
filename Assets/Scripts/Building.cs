using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using static Facade;
using TMPro;

interface IBuildingAction
{
    void OnBuildComplete(Town t);
    void OnBuildDestroy(Town t);
    void StartProduction();
}

public class Building : MonoBehaviour, IBuildingAction
{
    [SerializeField] private GameObject pickupGO;
    [SerializeField] private TextMeshPro progressTect;

    public BuildingData Data { get; set; }

    private List<BuildingAction> actions = new List<BuildingAction>();

    void Start()
    {
        progressTect.text = "";
    }

    public void Init(BuildingData data)
    {
        Data = data;

        foreach (var p in Data.properties) {
            actions.Add(BuildingAction.Create(this,p));
        }

        MakeBuilding(SetProgress);
    }

    public void SetProgress(float f)
    {
        progressTect.text = (int)(f * 100) + "%";
        progressTect.enabled = f != 1;
    }

    async Task MakeBuilding(Action<float> onProgress=null)
    {
        var f = Data.buildTime;
        while (f > 0) {
            await Task.Delay((int)(GM.Settings.IndicatorUpdateTime*1000));
            f = Mathf.Clamp(f - GM.Settings.IndicatorUpdateTime, 0, Data.buildTime);
            onProgress?.Invoke(1-f/ Data.buildTime);
            Debug.Log("MakeBuilding: "+ (1-f / Data.buildTime).ToString());
        }

        OnBuildComplete(GM.CurrentTown);
    }

    public void OnProductionComplete()
    {
        pickupGO.SetActive(true);
    }

    public void OnPickupRes()
    {
        pickupGO.SetActive(false);
        var p = Data.properties.Find(n=>n.propertyType==PropertyType.factory);
        GM.CurrentTown.AddRes(p.resPack);
        StartProduction();
    }

    public void OnBuildComplete(Town t) { foreach (var a in actions) a.OnBuildComplete(t); }
    public void OnBuildDestroy(Town t) { foreach (var a in actions) a.OnBuildDestroy(t); }
    public void StartProduction() { foreach (var a in actions) a.StartProduction(); }
}





