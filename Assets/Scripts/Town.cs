using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using static Facade;

public class Town : MonoBehaviour
{
    [SerializeField] private GameObject tilePrefab;
    [SerializeField] private Material prebuildMaterial;


    public Dictionary<ResourceType, int> Storage { get; } = new Dictionary<ResourceType, int>();
    public Dictionary<ResourceType, int> StorageMax { get; } = new Dictionary<ResourceType, int>();


    private bool IsDragEnable => GM.IsDragEnable && !IsPrebuildMode;
    private bool IsPrebuildMode => preBuildGO != null;

    private Bounds groundBounds;
    private Dictionary<Vector2Int, Building> buildings = new Dictionary<Vector2Int, Building>();

    void Start()
    {
        var tileObj = Instantiate(tilePrefab, transform, false);
        tileObj.transform.localScale = new Vector3(GM.Settings.TownSize.x,1, GM.Settings.TownSize.y);
        var mat = tileObj.GetComponentInChildren<Renderer>().material;
        mat.mainTextureScale = new Vector2(GM.Settings.TownSize.x, GM.Settings.TownSize.y);

        groundBounds = tileObj.GetComponentInChildren<Collider>().bounds;
    }

    private bool bDrag = false;
    private Vector3 dragLastPos;

    void Update()
    {
        if (bDrag) {
            if (Input.GetMouseButtonUp(0)) {
                bDrag = false;
            } else {
                if (GetRaycastPos(out Vector3 raycastPos)) {
                    var offs = raycastPos - dragLastPos;
                    transform.position += new Vector3(offs.x, 0, offs.z);
                    dragLastPos = raycastPos;
                }
            }
        } else {
            if (Input.GetMouseButtonDown(0)) {
                var res = GetRaycastResource();
                if (res!=null && IsDragEnable) {
                    var p = res.GetComponent<PickupRes>();
                    p.building.OnPickupRes();
                } else
                if (GetRaycastPos(out Vector3 raycastPos) && IsDragEnable) {
                    dragLastPos = raycastPos;
                    bDrag = true;
                }
            } else {
            }
        }

        if (IsPrebuildMode) {
            if (Input.GetMouseButtonDown(1)) {
                StopBuild();
            } else {
                if(GetRaycastPos(out Vector3 pos)) {
                    var xy = GetNearestLinkPoint(pos);
                    if (IsTileEmpty(xy)) {
                        preBuildGO.transform.localPosition = GetLinkPoint(xy);
                        preBuildGO.SetActive(true);
                    }
                }

                if(Input.GetMouseButtonDown(0) && preBuildGO.activeSelf) {
                    var xy = GetNearestLinkPoint(preBuildGO.transform.position);

                    var obj = Instantiate(preBuildData.prefab, transform, false);
                    obj.transform.localPosition = GetLinkPoint(xy);
                    var building = obj.GetComponent<Building>();
                    building.Init(preBuildData);
                    buildings[xy] = building;

                    foreach (var r in preBuildData.buildCost) {
                        Storage[r.resType] -= r.resValue;
                    }
                    (UI["Ingame"] as Ingame).UpdateData(this);

                    StopBuild();
                }
            }

        }
    }

    public void AddRes(ResourcePack resPack)
    {
        Storage[resPack.resType] = Mathf.Clamp(Storage[resPack.resType]+resPack.resValue,0, StorageMax[resPack.resType]);
        (UI["Ingame"] as Ingame).UpdateData(this);
    }

    private Vector2Int GetNearestLinkPoint(Vector3 pos)
    {
        var dx = groundBounds.size.x;// / GM.Settings.TownSize.x;
        var dy = groundBounds.size.z;// / GM.Settings.TownSize.y;
        pos -= transform.position + new Vector3(dx/2,0,dy/2);

        var x = Mathf.RoundToInt(pos.x / dx);
        var y = Mathf.RoundToInt(pos.z / dy);
        return new Vector2Int(x,y);
    }

    private Vector3 GetLinkPoint(Vector2Int pos)
    {
        var dx = groundBounds.size.x;// / GM.Settings.TownSize.x;
        var dy = groundBounds.size.z;// / GM.Settings.TownSize.y;
        return new Vector3(dx*pos.x+dx/2,0,dy*pos.y+dy/2) + groundBounds.center;
    }

    private bool GetRaycastPos(out Vector3 pos)
    {
        var r = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(r, out RaycastHit hit, 1000f, LayerMask.GetMask("Ground"))) {
            pos = hit.point;
            return true;
        }
        pos = Vector3.zero;
        return false;
    }

    private GameObject GetRaycastResource()
    {
        var r = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(r, out RaycastHit hit, 1000f, LayerMask.GetMask("Pickup"))) {
            return hit.collider.gameObject;
        }
        return null;
    }

    private bool IsTileEmpty(Vector2Int p)
    {
        return !buildings.ContainsKey(p);
    }

    private GameObject preBuildGO = null;
    private BuildingData preBuildData;
    public void StartBuild(BuildingData data)
    {
        preBuildData = data;
        preBuildGO = Instantiate(data.prefab,transform,false);

        var tmp = preBuildGO.GetComponentsInChildren<Renderer>();
        foreach(var r in tmp) {
            r.material = prebuildMaterial;
        }
        preBuildGO.SetActive(false);
    }

    private void StopBuild()
    {
        Destroy(preBuildGO);
        preBuildGO = null;
        UI["Build"].Hide();
        UI["Ingame"].Show();
    }
}
