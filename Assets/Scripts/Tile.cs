using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Facade;

public class Tile : MonoBehaviour
{
    [SerializeField] private Renderer _renderer;
    [SerializeField] private Material _matSelect;
    [SerializeField] private Material _matUnselect;

    public bool Select {
        set {
            _renderer.material = value ? _matSelect : _matUnselect;
        }
    }

    public Vector2Int Pos { get; set; }

    void Start()
    {
        
    }

    void Update()
    {
        
    }
}
