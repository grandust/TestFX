using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingActionStorage : BuildingAction
{
    public BuildingActionStorage(Building _building, BuildingProperty p)
    {
        props = p;
        building = _building;
    }

    public override void OnBuildComplete(Town t)
    {
        t.StorageMax[props.resPack.resType] += props.resPack.resValue;
    }

    public override void OnBuildDestroy(Town t)
    {
        t.StorageMax[props.resPack.resType] -= props.resPack.resValue;
    }
}