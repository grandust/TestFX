using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using static Facade;

public class BuildingActionFactory : BuildingAction
{
    public BuildingActionFactory(Building _building, BuildingProperty p)
    {
        props = p;
        building = _building;
    }

    public override void OnBuildComplete(Town t)
    {
        StartProduction();
    }

    public override void OnBuildDestroy(Town t)
    {
        //TODO: stop production
    }

    public override void StartProduction()
    {
        StartProductionAsync();
    }

    async void StartProductionAsync()
    {
        var f = props.factoryTime;
        while (f > 0) {
            await Task.Delay((int)(GM.Settings.IndicatorUpdateTime*1000));
            f = Mathf.Clamp(f - GM.Settings.IndicatorUpdateTime, 0, props.factoryTime);
            building.SetProgress(1 - f / props.factoryTime);
        }
        building.OnProductionComplete();
    }
}