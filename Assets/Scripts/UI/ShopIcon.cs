using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using static Facade;

public class ShopIcon : MonoBehaviour
{
    [SerializeField] private Text title;
    [SerializeField] private Text costGas;
    [SerializeField] private Text costMineral;
    [SerializeField] private GameObject priceGasGO;
    [SerializeField] private GameObject priceMinGO;
    [SerializeField] private Text time;
    [SerializeField] private Button buyButton;

    public BuildingData Data { get; set; }
    public Shop Shop { get; set; }

    public void Init()
    {
        title.text = Data.Title;

        var gData = Data.buildCost.Find(p => p.resType == ResourceType.gas);
        if (gData!=null) {
            costGas.text = gData.resValue.ToString();
        } else {
            priceGasGO.SetActive(false);
        }

        var mData = Data.buildCost.Find(p => p.resType == ResourceType.minerals);
        if (mData != null) {
            costMineral.text = mData.resValue.ToString();
        } else {
            priceMinGO.SetActive(false);
        }

        time.text = Data.buildTime + "s";

        buyButton.interactable = Data.buildCost.All(p => GM.CurrentTown.Storage[p.resType] >= p.resValue);
    }

    private void OnEnable()
    {
        if (Data != null) {
            buyButton.interactable = Data.buildCost.All(p => GM.CurrentTown.Storage[p.resType] >= p.resValue);
        }
    }

    public void OnBuildPress()
    {
        Shop.OnBuild(Data);
    }

}
