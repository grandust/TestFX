using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UIComponent : MonoBehaviour
{
    public void Show(Dictionary<string, object> dic = null)
    {
        gameObject.SetActive(true);
        OnShow(dic);
    }

    public void Hide()
    {
        OnHide();
        gameObject.SetActive(false);
    }

    protected abstract void OnShow(Dictionary<string, object> dic);
    protected abstract void OnHide();

    public bool IsVisible => gameObject.activeInHierarchy;
}
/*
public class PrefabAttribute : Attribute
{
    private string path;

    public PrefabAttribute(string s)
    {
        path = s;
    }

    public static string GetPrefabPath(Type T) => (Attribute.GetCustomAttribute(T, typeof(PrefabAttribute)) as PrefabAttribute)?.path;
}

*/