using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIMain : MonoBehaviour
{
    [SerializeField] private Transform uiHolder;
    [SerializeField] private UIComponent startUI;

    public static UIMain main;

    public UIComponent this[string s] => main.data[s];

    private Dictionary<string, UIComponent> data = new Dictionary<string, UIComponent>();

    RectTransform canvasRT;

    private void Awake()
    {
        canvasRT = gameObject.GetComponent<RectTransform>();

        foreach (Transform child in uiHolder) {
            data.Add(child.name, child.gameObject.GetComponent<UIComponent>());
            child.gameObject.SetActive(false);
        }

        main = this;
    }

    private void Start()
    {
        startUI?.Show();
    }

    public static bool IsPointerOverUI(Vector2 pos)
    {
        var eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = pos;
        var uiRaycaster = main.gameObject.GetComponent<GraphicRaycaster>();
        var results = new List<RaycastResult>();
        uiRaycaster.Raycast(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    public static Vector2 WorldToCanvasPosition(Vector3 position)
    {
        var temp = Camera.main.WorldToViewportPoint(position);

        temp.x *= main.canvasRT.sizeDelta.x;
        temp.y *= main.canvasRT.sizeDelta.y;

        temp.x -= main.canvasRT.sizeDelta.x * main.canvasRT.pivot.x;
        temp.y -= main.canvasRT.sizeDelta.y * main.canvasRT.pivot.y;

        return temp;
    }

}