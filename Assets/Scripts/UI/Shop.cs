using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static Facade;

public class Shop : UIComponent
{
    [SerializeField] private GameObject iconPrefab;
    [SerializeField] private GameObject iconHolder;
    [SerializeField] private Text TextGas;
    [SerializeField] private Text TextMineral;

    void Start()
    {
        foreach(var data in GM.Settings.buildings) {
            var obj = Instantiate(iconPrefab, iconHolder.transform, false);

            var icon = obj.GetComponent<ShopIcon>();
            icon.Shop = this;
            icon.Data = data;
            icon.Init();
        }
    }

    protected override void OnHide()
    {

    }

    protected override void OnShow(Dictionary<string, object> dic)
    {
        TextGas.text = GM.CurrentTown.Storage[ResourceType.gas].ToString();
        TextMineral.text = GM.CurrentTown.Storage[ResourceType.minerals].ToString();
    }

    public void OnBuild(BuildingData data)
    {
        UI["Shop"].Hide();
        UI["Build"].Show();

        GM.CurrentTown.StartBuild(data);
    }

    public void OnClosePress()
    {
        UI["Ingame"].Show();
        UI["Shop"].Hide();
    }
}
