using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static Facade;

public class Ingame : UIComponent
{
    [SerializeField] private Text TextGas;
    [SerializeField] private Text TextGasMax;
    [SerializeField] private Text TextMineral;
    [SerializeField] private Text TextMineralMax;

    protected override void OnHide()
    {

    }

    protected override void OnShow(Dictionary<string, object> dic)
    {
    }

    public void UpdateData(Town town)
    {
        TextGas.text = town.Storage[ResourceType.gas].ToString();
        TextMineral.text = town.Storage[ResourceType.minerals].ToString();
        TextGasMax.text = town.StorageMax[ResourceType.gas].ToString();
        TextMineralMax.text = town.StorageMax[ResourceType.minerals].ToString();
    }

    public void OnShopPress()
    {
        UI["Ingame"].Hide();
        UI["Shop"].Show();
    }

    public void OnCenter()
    {
        GM.CurrentTown.transform.position = Vector3.zero;
    }
}
